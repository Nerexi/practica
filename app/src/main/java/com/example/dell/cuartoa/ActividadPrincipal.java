package com.example.dell.cuartoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity {



    Button botonLogin, botonRegistrar, botonBuscar,botonPasarParametro,botonRecibirParametro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        botonLogin = (Button) findViewById(R.id.btnLogin);
        botonRegistrar = (Button) findViewById(R.id.btnIngresar);
        botonBuscar = (Button) findViewById(R.id.btnBuscar);
        botonPasarParametro= (Button) findViewById(R.id.btnPasarParametro);
        botonRecibirParametro= (Button) findViewById(R.id.btnRecibirParametro);


        botonLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {


            Intent intent = new Intent(ActividadPrincipal.this, ActividadLogin.class);
            startActivity(intent);
            }


            });
        botonRegistrar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ActividadPrincipal.this, ActividadRegistrar.class);
                startActivity(intent);
            }


        });
        botonBuscar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ActividadPrincipal.this, ActividadBuscar.class);
                startActivity(intent);
            }


        });
        botonPasarParametro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ActividadPrincipal.this, ActividadPasarParametro.class);
                startActivity(intent);
            }


        });
        botonRecibirParametro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ActividadPrincipal.this, ActividadRecibirParametro.class);
                startActivity(intent);
            }


        });

    }

    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                intent = new Intent(ActividadPrincipal.this, ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(ActividadPrincipal.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}

